package id.ac.ui.cs.advprog.midterm.controller;

import id.ac.ui.cs.advprog.midterm.domain.User;
import id.ac.ui.cs.advprog.midterm.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class UserController {

    private static final String USERS = "users";
    private static final String INDEX = "index";
    private static final String ADD_USER = "add-user";

    @Autowired
    UserRepository userRepository;

    @GetMapping("/signup")
    public String showSignUpForm(User user) {
        return ADD_USER;
    }

    @PostMapping("/adduser")
    public String addUser(@Valid User user, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return ADD_USER;
        }
        userRepository.save(user);
        model.addAttribute(USERS, userRepository.findAll());
        model.addAttribute("totalUsers", userRepository.count());
        model.addAttribute("totalFemale", userRepository.countByGender("Female"));
        model.addAttribute("totalMale", userRepository.countByGender("Male"));
        return INDEX;
    }

    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        model.addAttribute("user", user);
        return "update-user";
    }

    @PostMapping("/update/{id}")
    public String updateUser(@PathVariable("id") long id, @Valid User user,
                             BindingResult result, Model model) {
        User updatedUser = new User();
        updatedUser.setId(user.getId());
        updatedUser.setName(user.getName());
        updatedUser.setEmail(user.getEmail());
        if (result.hasErrors()) {
            updatedUser.setId(id);
            return "update-user";
        }
        userRepository.save(updatedUser);
        model.addAttribute(USERS, userRepository.findAll());
        return INDEX;
    }

    @GetMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") long id, Model model) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        userRepository.delete(user);
        model.addAttribute(USERS, userRepository.findAll());
        model.addAttribute("totalUsers", userRepository.count());
        model.addAttribute("totalFemale", userRepository.countByGender("Female"));
        model.addAttribute("totalMale", userRepository.countByGender("Male"));
        return INDEX;
    }

}