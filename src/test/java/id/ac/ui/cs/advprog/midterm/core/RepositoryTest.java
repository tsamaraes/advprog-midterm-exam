package id.ac.ui.cs.advprog.midterm.core;

import id.ac.ui.cs.advprog.midterm.domain.User;
import id.ac.ui.cs.advprog.midterm.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class RepositoryTest {

    @Autowired
    UserRepository userRepository;

    @Test
    public void checkTotalUsers() throws Exception {
        userRepository.save(new User("aku", "aku@mail.com", "Female"));
        assertEquals(1, userRepository.count());
    }
}
