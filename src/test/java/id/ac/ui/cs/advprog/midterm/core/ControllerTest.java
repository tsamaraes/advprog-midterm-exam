package id.ac.ui.cs.advprog.midterm.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import id.ac.ui.cs.advprog.midterm.domain.User;
import id.ac.ui.cs.advprog.midterm.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private BindingResult bindingResult;

    @Mock
    private Model model;

    @MockBean
    private UserRepository userRepository;

    private static User user = new User();

    @BeforeEach
    void createUser() {
        User user = new User();
        user.setId(24);
        user.setName("Sarah");
        user.setEmail("sarah@mail.com");
        user.setGender("Female");
        when(userRepository.findById(user.getId()))
                .thenReturn(java.util.Optional.ofNullable(user));
    }

    @Test
    public void checkSignUpTemplate() throws Exception {
        this.mockMvc.perform(get("/signup"))
                .andExpect(status().isOk())
                .andExpect(view().name("add-user"));
    }
    @Test
    public void checkAddValidUserTemplate() throws Exception {
        user.setId(24);
        user.setName("Sarah");
        user.setEmail("sarah@mail.com");
        user.setGender("Female");
        this.mockMvc.perform(post("/adduser")
                .flashAttr("user", user)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }

    @Test
    public void checkAddInvalidUserTemplate() throws Exception {
        user.setEmail(null);
        this.mockMvc.perform(post("/adduser")
                .flashAttr("user", user)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(status().isOk())
                .andExpect(view().name("add-user"));
    }

    @Test
    public void checkEditUserById() throws Exception {
        user.setId(24);
        user.setName("Sarah");
        user.setEmail("sarah@mail.com");
        user.setGender("Female");
        this.mockMvc.perform(get("/edit/" + 24))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("user"))
                .andExpect(view().name("update-user"));
    }

    @Test
    public void checkEditUserByIdInvalid() throws Exception {
        try {
            this.mockMvc.perform(get("/edit/99"))
                    .andExpect(status().isOk())
                    .andExpect(view().name("update-user"))
                    .andExpect(model().attributeExists("user"));
        } catch (Exception e) {
            assertEquals("Request processing failed; nested exception is java.lang.IllegalArgumentException: Invalid user Id:99", e.getMessage());
        }
    }

    @Test
    public void checkUpdateUserByIdValid() throws Exception {
        user.setId(24);
        user.setName("Sarah");
        user.setEmail("sarah@mail.com");
        user.setGender("Female");
        this.mockMvc.perform(post("/update/" + user.getId())
                .flashAttr("user", user)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }

    @Test
    public void checkUpdateUserByIdInvalid() throws Exception {
        user.setEmail(null);
        this.mockMvc.perform(post("/update/" + 24)
                .flashAttr("user", user)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(status().isOk())
                .andExpect(view().name("update-user"));
    }

    @Test
    public void checkDeleteUserByIdValid() throws Exception {
        user.setId(24);
        this.mockMvc.perform(get("/delete/" + user.getId()))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }

    @Test
    public void checkDeleteUserByIdInvalid() throws Exception {
        try {
            this.mockMvc.perform(get("/delete/99"))
                    .andExpect(status().isOk())
                    .andExpect(view().name("index"));
        } catch (Exception e) {
            assertEquals("Request processing failed; nested exception is java.lang.IllegalArgumentException: Invalid user Id:99", e.getMessage());
        }

    }

}