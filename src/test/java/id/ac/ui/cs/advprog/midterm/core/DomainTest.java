package id.ac.ui.cs.advprog.midterm.core;

import id.ac.ui.cs.advprog.midterm.domain.User;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;

class DomainTest {

    @Autowired
    private static User user = new User();

    @Test
    void checkNameExist() {
        String temp = "Sarah";
        user.setName(temp);
        assertEquals("Sarah", user.getName());
    }

    @Test
    void checkMailExist() {
        String temp = "sarah@mail.com";
        user.setEmail(temp);
        assertEquals("sarah@mail.com", user.getEmail());
    }

    @Test
    void checkIdExist() {
        long user_id = 12345;
        user.setId(user_id);
        assertEquals(user.getId(), user_id);
    }

    @Test
    void checkGenderExist() {
        String gender = "Female";
        user.setGender(gender);
        assertEquals("Female", user.getGender());
    }

    @Test
    void checkUserToString() {
        user.setId(2404);
        user.setName("Sarah");
        user.setEmail("sarah@mail.com");
        user.setGender("Female");
        assertEquals("User{id=2404, name='Sarah', email='sarah@mail.com', gender='Female'}", user.toString());
    }

}
